/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	// full name

	function printWelcomeMessage(){
	let firstName = prompt('Enter your first name:');
	let lastName = prompt('Enter your last name:');

	console.log('Hi ' + firstName + " " + lastName + ".");
	};
	printWelcomeMessage();


	// age

	let agePrompt = prompt("Enter your age:" + ".");
	console.log("You are " + agePrompt + " years old.");

	// location

	let locationPrompt = prompt("Enter your location:");
	console.log("You live in " + locationPrompt + ".");


/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/	// let Artists = ['Paramore,', 'Panic! At The Disco', 'Twenty one pilots', 'OneRepublic', 'Fall Out Boy'];
	// console.log(Artists);



	function favoriteMartists(){
		console.log("1. Paramore");
		console.log("2. Panic! At The Disco");
		console.log("3. Twenty one pilots");
		console.log("4. OneRepublic");
		console.log("5. Fall Out Boy");
	};
	favoriteMartists();



/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function movie1(){
		console.log("1. Hunger Games");
		console.log("Rotten Tomatoes Rating: 90%")
	};
	movie1();

	function movie2(){
		console.log("2. Divergent");
		console.log("Rotten Tomatoes Rating: 41%")
	};
	movie2();

	function movie3(){
		console.log("3. Home Alone");
		console.log("Rotten Tomatoes Rating: 67%")
	};
	movie3();

	function movie4(){
		console.log("4. The Sixth Sense");
		console.log("Rotten Tomatoes Rating: 86%")
	};
	movie4();

	function movie5(){
		console.log("5. Shutter Island");
		console.log("Rotten Tomatoes Rating: 68%")
	};
	movie5();


/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){

		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with: ");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
		};

	printFriends();
